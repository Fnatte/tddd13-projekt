package nu.matteus.shoppingcartexample;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import nu.matteus.swing.shoppingcart.DefaultShoppingCartModel;
import nu.matteus.swing.shoppingcart.ShoppingCart;
import nu.matteus.swing.shoppingcart.ShoppingCartItem;

public class ShoppingCartExampleWindow extends JFrame {
	
	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 7423777186677881339L;
	
	/**
	 * Content pane of the frame.
	 */
	private JPanel contentPane;
	
	public ShoppingCartExampleWindow() throws Exception {
		
		// Setup content pane
		contentPane = (JPanel)this.getContentPane();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		contentPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		
		// Create ShoppingCart
		ShoppingCart shoppingCart = new ShoppingCart();
		contentPane.add(shoppingCart);
		
		// Add items
		DefaultShoppingCartModel model = (DefaultShoppingCartModel)shoppingCart.getModel();
		model.add(new ShoppingCartItem("Pelle", 1, 10.50));
		model.add(new ShoppingCartItem("Kondi", 1, 15.20));
		
		this.setLocationByPlatform(true);
		this.setTitle("Shopping Cart Example");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		// this.pack();
		this.setSize(700, 540);
		
		this.setVisible(true);
	}
	
}
