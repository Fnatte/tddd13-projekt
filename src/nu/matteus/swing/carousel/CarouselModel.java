package nu.matteus.swing.carousel;

import java.util.ListIterator;

/**
 * Model interface for <code>Carousel</code> objects.
 * 
 * @author Matteus
 *
 */
public interface CarouselModel extends Iterable<Object> {
	
	/**
	 * Gets a iterator on every item on the model starting at the specified value.
	 * @param index The start index.
	 * @return Returns a <code>ListIterator</code> of <code>Object</code>s.
	 */
	public ListIterator<Object> listIterator(int index);
	
	/**
	 * Gets a iterator on every item on the model.
	 * @return Returns a <code>ListIterator</code> of <code>Object</code>s.
	 */
	public ListIterator<Object> listIterator();
	
	/**
	 * Gets the number of items.
	 * @return Returns the number of items in this model.
	 */
	public int size();
}
