package nu.matteus.swing.carousel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * A container that contains a toolbar panel and page indicator for a specified carousel.
 * The Carousel is not created by this class itself and must be provided using the setCarousel method.
 * 
 * @author Matteus
 *
 */
public class BasicCarouselContainer extends JPanel {

	/**
	 * Generated serial verion UID
	 */
	private static final long serialVersionUID = -4465190076112021971L;
	
	/**
	 * Buttons that are placed on the toolbar.
	 * The buttons use custom icons and provide ability to navigate on the carousel.
	 */
	private JButton btnNext, btnPrev;
	
	/**
	 * Carousel object to hold the current carousel.
	 */
	private Carousel carousel;
	
	/**
	 * Page indicator that displays on what page the carousel is currently on.
	 */
	private CarouselPageIndicator pageIndicator;
	
	/**
	 * Creates a new BasicCarouselContainer.
	 */
	public BasicCarouselContainer() {
		
		// Set layout
		this.setLayout(new BorderLayout());
		
		// Toolbar
		JPanel toolbarPane = new JPanel();
		toolbarPane.setLayout(new BorderLayout());
		this.add(toolbarPane, BorderLayout.PAGE_START);
		
		// Indicator
		JPanel pageIndicatorPane = new JPanel();
		pageIndicator = new CarouselPageIndicator();
		pageIndicatorPane.setLayout(new GridBagLayout());
		pageIndicatorPane.add(pageIndicator);
		toolbarPane.add(pageIndicatorPane, BorderLayout.LINE_START);
		
		
		// Toolbar buttons
		JPanel buttonPane = new JPanel(new FlowLayout());
		Dimension btnDim = new Dimension(20, 25);
		
		btnPrev = new JButton(new ImageIcon(getClass().getResource("arrow-left.png")));
		btnPrev.setPressedIcon(new ImageIcon(getClass().getResource("arrow-left-pressed.png")));
		btnPrev.setRolloverIcon(new ImageIcon(getClass().getResource("arrow-left-hover.png")));
		btnPrev.setBorderPainted(false);
		btnPrev.setFocusPainted(false);
		btnPrev.setContentAreaFilled(false);
		btnPrev.setPreferredSize(btnDim);
		
		btnNext = new JButton(new ImageIcon(getClass().getResource("arrow-right.png")));
		btnNext.setPressedIcon(new ImageIcon(getClass().getResource("arrow-right-pressed.png")));
		btnNext.setRolloverIcon(new ImageIcon(getClass().getResource("arrow-right-hover.png")));
		btnNext.setBorderPainted(false);
		btnNext.setFocusPainted(false);
		btnNext.setContentAreaFilled(false);
		btnNext.setPreferredSize(btnDim);
		
		buttonPane.add(btnPrev);
		buttonPane.add(btnNext);
		
		toolbarPane.add(buttonPane, BorderLayout.LINE_END);
		
		addButtonListeners();
	}
	
	/**
	 * Creates a new <code>BasicCarouselContainer</code> and sets its
	 * <code>Carousel</code> to operate on.
	 * 
	 * @param carousel The <code>Carousel</code> to operate on.
	 */
	public BasicCarouselContainer(Carousel carousel) {
		this();
		setCarousel(carousel);
	}

	/**
	 * Gets the current Carousel presented by the BasicCarouselContainer.
	 * @return The current Carousel.
	 */
	public Carousel getCarousel() {
		return carousel;
	}

	/**
	 * Sets the Carousel object that the BasicCarouselContainer should operate on.
	 * 
	 * @param carousel The Carousel object to assign with this BasicCarouselContainer
	 */
	public void setCarousel(Carousel carousel) {
		
		// Remove previous carousel
		if(this.carousel != null) remove(this.carousel);
		
		// Set and add the new
		this.carousel = carousel;
		this.pageIndicator.setCarousel(carousel);
		addCarouselListeners();
		add(carousel, BorderLayout.CENTER);
	}
	
	/**
	 * Adds action listeners to the buttons.
	 */
	private void addButtonListeners() {
		btnNext.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getCarousel().Next();
			}
		});
		btnPrev.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getCarousel().Previous();
			}
		});
	}
	
	/**
	 * Adds a CarouselListener to the current Carousel.
	 * Used to know when to repaint the page indicator.
	 */
	private void addCarouselListeners() {
		carousel.addCarouselListener(new CarouselListener() {
			@Override
			public void onCarouselPageChanged(Carousel sender, int newIndex) {
				pageIndicator.repaint();
			}

			@Override
			public void onCarouselItemClicked(Carousel carousel, Object o) {
				
			}
		});
	}
}	
