package nu.matteus.swing.carousel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.UIManager;

/**
 * Renderer that can be used by a <code>Carousel</code> to paint items.
 * 
 * @author Matteus
 *
 */
public class DefaultCarouselItemRenderer extends CarouselItemRenderer {
	
	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 9152326519862936512L;
	
	/**
	 * Current value to be painted.
	 */
	private Object value;
	
	/**
	 * Determines whether the value is selected.
	 * This field is not currently used.
	 */
	private boolean isSelected;
	
	/**
	 * The amount of top margin. 
	 */
	private int imageTopMargin = 10;
	
	/**
	 * Creates a new <code>DefaultCarouselItemRenderer</code>.
	 */
	public DefaultCarouselItemRenderer() {
		setFont(UIManager.getDefaults().getFont("Label.font"));
	}
	
	/**
	 * Paints the current value on the specified <code>Graphics</code> context. 
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		g.setColor(isSelected ? Color.BLUE : Color.BLACK);
		
		if(value instanceof DefaultCarouselItem) {
			DefaultCarouselItem item = (DefaultCarouselItem)value;
			paintItem(item, g);
		}
	}
	
	/**
	 * Used to paint the specified <code>DefaultCarouselItem</code> on the specified <code>Graphics</code> context.
	 * @param item The item to be drawn.
	 * @param g The graphics context to draw onto.
	 */
	public void paintItem(DefaultCarouselItem item, Graphics g) {
		
		Font font = getFont();
		FontMetrics fontMetrics = getFontMetrics(font);
		
		int titleHeight = fontMetrics.getHeight();
		
		// Title
		g.drawString(item.getTitle(), 0, titleHeight);
		
		// Image
		BufferedImage image = item.getImage();
		if(image != null) {
			g.drawImage(image, 0, titleHeight + imageTopMargin, image.getWidth(), image.getHeight(), null);
		}
	}
	
	/**
	 * Gets the calculated width of the current value. 
	 */
	@Override
	public int getWidth() {
		
		if(value instanceof DefaultCarouselItem) {
			DefaultCarouselItem item = (DefaultCarouselItem)value;
			Font font = getFont();
			FontMetrics fontMetrics = getFontMetrics(font);
			
			int titleWidth = fontMetrics.stringWidth(item.getTitle());
			
			int imageWidth = item.getImage() != null ? item.getImage().getWidth() : 0;
			
			return Math.max(titleWidth, imageWidth);
		}
		
		return super.getWidth();
	}
	
	/**
	 * Gets the calculated height on the current value.
	 */
	@Override
	public int getHeight() {
		if(value instanceof DefaultCarouselItem) {
			DefaultCarouselItem item = (DefaultCarouselItem)value;
			Font font = getFont();
			FontMetrics fontMetrics = getFontMetrics(font);
			
			int height = fontMetrics.getHeight();
			
			if(item.getImage() !=  null) {
				height += item.getImage().getHeight() + imageTopMargin;
			}
			
			return height;
		}
		
		return super.getWidth();
	}

	/**
	 * Not implemented.
	 * Returns NULL.
	 */
	@Override
	public Component getComponent() {
		return null;
	}

	/**
	 * Sets a value that this renderer should use. 
	 */
	@Override
	public void setValue(Object aValue, boolean isSelected) {
		this.value = aValue;
		this.isSelected = isSelected;
	}
	
}
