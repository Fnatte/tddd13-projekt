package nu.matteus.swing.carousel;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

/**
 * A default <code>Carousel</code> item with title, description and (thumbnail) image.
 * Must be used with the Carousel if the default renderer class is used.
 * 
 * @author Matteus
 *
 */
public class DefaultCarouselItem {
	
	/*
	 * Fields
	 */
	
	private BufferedImage image;
	private String title;
	private String description;
	
	
	/*
	 * Constructors	
	 */
	
	public DefaultCarouselItem() {}
	
	public DefaultCarouselItem(String title) {
		setTitle(title);
	}
	
	public DefaultCarouselItem(String title, String description) {
		setTitle(title);
		setDescription(description);
	}
	
	public DefaultCarouselItem(String title, String description, String imagePath) throws IOException {
		setTitle(title);
		setDescription(description);
		setImage(imagePath);
	}
	
	
	/*
	 * Getters & Setters
	 */
	
	public BufferedImage getImage() {
		return image;
	}
	public void setImage(BufferedImage image) {
		this.image = image;
	}
	public void setImage(String imagePath) throws IOException {
		URL url = this.getClass().getResource(imagePath);
		this.image = ImageIO.read(url);
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return this.title;
	}
	
}
