package nu.matteus.swing.carousel;

import java.util.EventListener;

/**
 * A event listener interface that can be added to a <code>Carousel</code>
 * to be notified on Carousel specific events.
 * @author Matteus
 *
 */
public interface CarouselListener extends EventListener {
	
	/**
	 * Is fired when a <code>Carousel</code> have navigated to a new page.
	 * @param sender The <code>Carousel</code> that changed page.
	 * @param newIndex The new index of the page on the <code>Carousel</code>.
	 */
	public void onCarouselPageChanged(Carousel sender, int newIndex);

	/**
	 * Is fired when a item on a <code>Carousel</code> was clicked.
	 * 
	 * @param carousel The <code>Carousel</code> that the item was contained on.
	 * @param o The item object that was clicked.
	 */
	public void onCarouselItemClicked(Carousel carousel, Object o);
	
}
