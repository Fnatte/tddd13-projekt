package nu.matteus.swing.carousel;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.Vector;

import javax.swing.JComponent;

/**
 * A component that arranges a set of items on a horizontal line.
 * The component can cap the number of items to be viewed at a time
 * and has methods that allow navigation to the other items.
 * 
 * @author Matteus
 *
 */
public class Carousel extends JComponent implements DragSourceListener, DragGestureListener, Transferable {
	
	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 4768113919493207105L;
	
	/**
	 * The number of visible items to be displayed.
	 */
	private int numberOfVisibleItems = 3;
	
	/**
	 * The current page index.
	 */
	private int currentPageIndex = 0;
	
	/**
	 * Current carousel model.
	 */
	private CarouselModel model;
	
	/**
	 * Renderer to be used when painting each item.
	 */
	private CarouselItemRenderer renderer;
	
	/**
	 * The UI that will determine and calculate the layout.
	 */
	private CarouselUI ui;
	
	/**
	 * A collection of listeners interested in events on this specific carousel events.
	 */
	private Vector<CarouselListener> listeners = new Vector<CarouselListener>();
	
	/**
	 * Drag Source
	 */
	private DragSource dragSource;
	
	/**
	 * Currently dragged object
	 */
	private Object currentlyDraggedObject;
	
	/**
	 * Creates a new Carousel. 
	 */
	public Carousel()
	{
		ui = new CarouselUI();
		model = new DefaultCarouselModel();
		renderer = new DefaultCarouselItemRenderer();
		
		addMouseListener();
		addDragDropSupport();
		
		setUI(ui);
	}

	private void addDragDropSupport() {

		// DragSource
		dragSource = new DragSource();
		dragSource.createDefaultDragGestureRecognizer(
			this, DnDConstants.ACTION_COPY, this);
		
	}

	/**
	 * Adds a mouse listener to the carousel.
	 * Used to capture mouse clicks that will be used to fire "item clicked"-events.
	 */
	private void addMouseListener() {
		addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {}
			
			@Override
			public void mousePressed(MouseEvent e) {}
			
			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				Object o = getItemAt(e.getX(), e.getY());
				if(o != null) {
					onCarouselItemClicked(o);
				}
			}
		});
	}
	
	/**
	 * Finds what item that is currently displayed on the speicifed coordinates.
	 * @param x X coordinate.
	 * @param y Y coordinate.
	 * @return Will return the item found at the specified location or NULL if no item was found.
	 */
	public Object getItemAt(int x, int y) {
		return ui.getItemAt(x, y);
	}

	/**
	 * Navigates to the next page, or the first page if the last page had been reached.
	 */
	public void Next()
	{
		setCurrentPageIndex(getCurrentPageIndex()+1);
	}

	/**
	 * Navigates to the previous page or the last page if the first page was the current.
	 */
	public void Previous()
	{
		setCurrentPageIndex(getCurrentPageIndex()-1);
	}

	/**
	 * Gets the number of visible items.
	 * 
	 * @return The number of visible items.
	 */
	public int getNumberOfVisibleItems() {
		return numberOfVisibleItems;
	}

	/**
	 * Sets the number of visible items.
	 * 
	 * @param numberOfVisibleItems The number of items to be displayed.
	 * @throws Exception Thrown if the specified value was negative.
	 */
	public void setNumberOfVisibleItems(int numberOfVisibleItems) throws Exception {
		
		if(numberOfVisibleItems < 0) {
			throw new Exception("Cannot display a negative amount of items.");
		}
		
		this.numberOfVisibleItems = numberOfVisibleItems;
	}
	
	/**
	 * Gets the number of pages on the carousel.
	 * @return The page count.
	 */
	public int getPageCount() {
		return (int)Math.ceil(getModel().size() / (double)numberOfVisibleItems);
	}
	
	/**
	 * Adds a CarouselListener that will be called on Carousel specific events.
	 * @param listener The listener to be notified on carousel events.
	 */
	public void addCarouselListener(CarouselListener listener) {
		listeners.add(listener);
	}
	
	/**
	 * Removes any previously added CarouselListener from the carousel event subscription.
	 * @param listener The listener to be unsubscribed.
	 */
	public void removeCarouselListener(CarouselListener listener) {
		listeners.remove(listener);
	}
	
	/* Events */
	/**
	 * Fires a page changed event.
	 */
	protected void onCarouselPageChanged() {
		for(CarouselListener listener : listeners) {
			listener.onCarouselPageChanged(this, getCurrentPageIndex());
		}
	}
	
	/**
	 * Fires a item clicked event with the specified object.
	 * @param o The object that was clicked.
	 */
	protected void onCarouselItemClicked(Object o) {
		for(CarouselListener listener : listeners) {
			listener.onCarouselItemClicked(this, o);
		}
	}
	
	
	/*
	 * Getters and setters
	 */
	
	/**
	 * Gets the carousel model.
	 * @return Returns the carousel model.
	 */
	public CarouselModel getModel() {
		return model;
	}

	/**
	 * Sets the carousel model to be used by this Carousel.
	 * @param model The new carousel model.
	 */
	public void setModel(CarouselModel model) {
		this.model = model;
	}

	/**
	 * Gets the renderer that is used to paint each item on the carousel.
	 * @return Returns the currently used item renderer.
	 */
	public CarouselItemRenderer getRenderer() {
		return renderer;
	}

	/**
	 * Sets a new renderer that should be used to paint every item on the carousel.
	 * @param renderer The new renderer to be used.
	 */
	public void setRenderer(CarouselItemRenderer renderer) {
		this.renderer = renderer;
	}

	/**
	 * Gets the current page index.
	 * @return Returns the current page index.
	 */
	public int getCurrentPageIndex() {
		return currentPageIndex;
	}

	/**
	 * Sets and navigates to the specified index.
	 * If the specified index is higher that the last index, the index of the first page will be used.
	 * Likewise, if the specified index is lower the the first index, the last availble index will be used.
	 *  
	 * @param currentPageIndex The new page index to navigate to.
	 */
	public void setCurrentPageIndex(int currentPageIndex) {
		
		CarouselModel model = getModel();
		int count = model.size();
		
		if(currentPageIndex < 0) {
			// Set to last page
			this.currentPageIndex = getPageCount() - 1; 
		} else if(currentPageIndex * numberOfVisibleItems >= count) {
			// Set to first page
			this.currentPageIndex = 0;
		} else {
			this.currentPageIndex = currentPageIndex;
		}
		
		onCarouselPageChanged();
		repaint();
	}

	@Override
	public Object getTransferData(DataFlavor flavor)
			throws UnsupportedFlavorException, IOException {
		
		if(flavor.getHumanPresentableName() == "Object") {
			return currentlyDraggedObject;
		}
		
		throw new UnsupportedFlavorException(flavor);
	}

	@Override
	public DataFlavor[] getTransferDataFlavors() {
		return new DataFlavor[] {
			new DataFlavor(Object.class, "Object")
		};
	}

	@Override
	public boolean isDataFlavorSupported(DataFlavor flavor) {
		DataFlavor[] flavors = getTransferDataFlavors();
		for(DataFlavor f : flavors) {
			if(f.equals(flavor)) return true;
		}
		return false;
	}

	@Override
	public void dragGestureRecognized(DragGestureEvent dge) {
		
		if(dge.getTriggerEvent() instanceof MouseEvent) {
			MouseEvent mouseEvent = (MouseEvent) dge.getTriggerEvent();
			
			currentlyDraggedObject = getItemAt(mouseEvent.getX(), mouseEvent.getY());
			
			dragSource.startDrag(
				dge, DragSource.DefaultMoveDrop, 
				this, this
			);
		}
	}

	@Override
	public void dragDropEnd(DragSourceDropEvent dsde) {
		repaint();
	}

	@Override
	public void dragEnter(DragSourceDragEvent dsde) {}

	@Override
	public void dragExit(DragSourceEvent dse) {}

	@Override
	public void dragOver(DragSourceDragEvent dsde) {}

	@Override
	public void dropActionChanged(DragSourceDragEvent dsde) {}
}
