package nu.matteus.swing.carousel;

import javax.swing.JComponent;
import javax.swing.Renderer;

/**
 * 
 * Abstract class to be used by a <code>Carousel</code> to render every item on the <code>Carousel</code>. 
 * 
 * @author Matteus
 *
 */
public abstract class CarouselItemRenderer extends JComponent implements Renderer {

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = -9114406785341743384L;
	
}