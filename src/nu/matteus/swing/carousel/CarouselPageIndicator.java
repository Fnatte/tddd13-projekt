package nu.matteus.swing.carousel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JComponent;

/**
 * A component that indicates what page a specified <code>Carousel</code> currently is displaying.
 * This is done by displaying a circle for each page, and highlighting the circle of the current page.
 * 
 * @author Matteus
 *
 */
public class CarouselPageIndicator extends JComponent {

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = -8420152840821220632L;
	
	/**
	 * Carousel object to display the current page for.
	 */
	private Carousel carousel;
	
	/**
	 * The radius on the circles painted.
	 */
	private int radius = 6;
	
	/**
	 * Padding between each circle.
	 */
	private int padding = 3;

	/**
	 * Creates a new <code>CarouselPageIndicator</code>.
	 */
	public CarouselPageIndicator() {
		
	}
	
	/**
	 * Gets the currently used <code>Carousel</code> by this <code>CarouselPageIndicator</code>.
	 * @return The currently used <code>Carousel</code>.
	 */
	public Carousel getCarousel() {
		return carousel;
	}

	/**
	 * Sets what <code>Carousel</code> to display pages for. 
	 * @param carousel The new <code>Carousel</code> to be used.
	 */
	public void setCarousel(Carousel carousel) {
		this.carousel = carousel;
	}
	
	/**
	 * Paints the component with one circle for each page on the <code>Carousel</code> on a horizontal line.
	 */
	@Override
	public void paint(Graphics g) {
		
		int width = radius * 2;
		int height = radius * 2;
		int currentPage = carousel.getCurrentPageIndex();
		int pageCount = carousel.getPageCount();
		
		for(int n = 0; n < pageCount; n++) {
			g.setColor(currentPage == n ? Color.GRAY : Color.LIGHT_GRAY);
			g.fillOval(n * (width + padding), 0, width, height);
			
		}
	}
	
	/**
	 * Gets the preferred size by this component.
	 */
	@Override
	public Dimension getPreferredSize() {
		int pageCount = carousel.getPageCount();
		return new Dimension(
			pageCount * (radius * 2 + padding),
			radius * 2 + padding
		);
	}
	
}
