package nu.matteus.swing.carousel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ListIterator;

import javax.swing.CellRendererPane;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;

/**
 * A component UI class that calculates layout for a <code>Carousel</code>.
 * 
 * @author Matteus
 *
 */
public class CarouselUI extends ComponentUI {
	
	/**
	 * Carousel in use.
	 */
	private Carousel carousel;
	
	/**
	 * The container that is used to render each item on. 
	 */
	private CellRendererPane rendererPane;
	
	/**
	 * The amount of padding between each item.
	 */
	private int cellPadding = 30;
	
	/**
	 * Determines whether to draw a separation line between each item.
	 */
	private boolean drawSeparator = true;
	
	/**
	 * The color on the line drawn between each item.
	 */
	private Color separatorColor = Color.LIGHT_GRAY;
	
	/**
	 * Paints every item on the carousel.
	 */
	@Override
	public void paint(Graphics g, JComponent c) {
		
		CarouselItemRenderer renderer = carousel.getRenderer();
		CarouselModel model = carousel.getModel();

		// Render all items
		int visibleItems = carousel.getNumberOfVisibleItems();
		
		int startIndex = carousel.getCurrentPageIndex() * visibleItems;
		ListIterator<Object> it = model.listIterator(startIndex);
		int x = 0;
		while(it.hasNext() && it.nextIndex() - startIndex < visibleItems) {
			Object o = it.next();
			renderer.setValue(o, false);
			
			int itemWidth = renderer.getWidth();
			int itemHeight = renderer.getHeight();
			
			rendererPane.paintComponent(g, renderer, carousel, x, 0, itemWidth, itemHeight, true);
			
			if(drawSeparator && it.hasNext()) {
				int separatorX = x + itemWidth + cellPadding / 2;
				g.setColor(separatorColor);
				g.drawLine(separatorX, 0, separatorX, itemHeight);
			}
			
			x += itemWidth + cellPadding;
		}
		
		rendererPane.removeAll();
	}
	
	/**
	 * Sets what <code>Carousel</code> this <code>CarouselUI</code> will provide layout for. 
	 */
	@Override
	public void installUI(JComponent c) {
		super.installUI(c);
		
		carousel = (Carousel)c;
		
		rendererPane = new CellRendererPane();
		carousel.add(rendererPane);
	}
	
	/**
	 * Removes the association with the previously used <code>Carousel</code>.
	 */
	@Override
	public void uninstallUI(JComponent c) {
		super.uninstallUI(c);
		
		carousel.remove(rendererPane);
		carousel = null;
		rendererPane = null;
	}
	
	/**
	 * Gets the calculated size and space this UI will take up.  
	 */
	@Override
	public Dimension getPreferredSize(JComponent c) {
		
		CarouselModel model = carousel.getModel();
		CarouselItemRenderer renderer = carousel.getRenderer();
		
		// Get the max width and height
		int maxWidth = 0, maxHeight = 0;
		for(Object o : model) {
			if(o instanceof DefaultCarouselItem) {
				DefaultCarouselItem item = (DefaultCarouselItem)o;
				renderer.setValue(item, false);
				int w = renderer.getWidth();
				int h = renderer.getHeight();
				if(w > maxWidth) maxWidth = w;
				if(h > maxHeight) maxHeight = h;
			}
		}
		
		return new Dimension(carousel.getNumberOfVisibleItems() * maxWidth, maxHeight);
	}
	
	/**
	 * Gets the item that this layout have drawn at the specified coordinates.
	 * @param x The X-coordinate.
	 * @param y The Y-coordinate.
	 * @return Returns the item found on the specified location. If no item was found, NULL is returned.
	 */
	public Object getItemAt(int x, int y) {
		
		if(x < 0 || y < 0) return null;
		
		CarouselItemRenderer renderer = carousel.getRenderer();
		CarouselModel model = carousel.getModel();

		int visibleItems = carousel.getNumberOfVisibleItems();
		
		int startIndex = carousel.getCurrentPageIndex() * visibleItems;
		ListIterator<Object> it = model.listIterator(startIndex);
		int curX = -cellPadding/2;
		while(it.hasNext() && it.nextIndex() - startIndex < visibleItems) {
			Object o = it.next();
			renderer.setValue(o, false);
			int nextX = curX + renderer.getWidth() + cellPadding;
			
			if(x >= curX && x <= nextX && y >= 0 && y <= renderer.getHeight()) {
				return o;
			}
			
			curX = nextX;
		}
		
		return null;
	}
	
}
