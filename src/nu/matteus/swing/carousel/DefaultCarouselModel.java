package nu.matteus.swing.carousel;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;

/**
 * Default <code>Carousel</code> model that allows some collection manipulation and iteration.
 * A thread safe Vector is used underneath.
 * 
 * @author Matteus
 *
 */
public class DefaultCarouselModel implements CarouselModel {
	
	/**
	 * Vector to hold the items.
	 */
	private Vector<Object> items;
	
	/**
	 * Creates a new instance of a <code>DefaultCarouselModel</code>.
	 * 
	 * @param collection A collection of items to initialise the collection with.
	 */
	public DefaultCarouselModel(Collection<? extends Object> collection)
	{
		items = new Vector<>(collection);
	}
	
	/**
	 * Creates a new instance of a <code>DefaultCarouselModel</code>.
	 */
	public DefaultCarouselModel()
	{
		items = new Vector<>();
	}
	
	/**
	 * Adds a item to the model.
	 * @param o Item to add.
	 */
	public void Add(Object o) {
		this.items.add(o);
	}
	
	/**
	 * Returns a enumeration that enumerates over every item.
	 * @return
	 */
	public Enumeration<Object> elements()
	{
		return this.items.elements();
	}
	
	/**
	 * Gets a iterator on every item on the model starting at the specified value.
	 * @param index The start index.
	 * @return Returns a <code>ListIterator</code> of <code>Object</code>s.
	 */
	public ListIterator<Object> listIterator(int index) {
		return this.items.listIterator(index);
	}
	
	/**
	 * Gets a iterator on every item on the model.
	 * @return Returns a <code>ListIterator</code> of <code>Object</code>s.
	 */
	public ListIterator<Object> listIterator() {
		return this.items.listIterator();
	}
	
	/**
	 * Gets the number of items.
	 * @return Returns the number of items in this model.
	 */
	public int size() {
		return items.size();
	}

	/**
	 * Returns a iterator.
	 */
	@Override
	public Iterator<Object> iterator() {
		return listIterator();
	}
	
}
