package nu.matteus.swing.shoppingcart;

/**
 * Item used by <code>ShoppingCart</code> and its model
 * that represents a product row in the <code>ShoppingCart</code>.
 * 
 * 
 * @author Matteus
 *
 */
public class ShoppingCartItem {
	
	/* 
	 * Fields
	 */
	
	private String title;
	private int quantity;
	private double price;
	
	/*
	 * Constructors
	 */
	
	/**
	 * Creates a new empty instance of a <code>ShoppingCartItem</code>.
	 */
	public ShoppingCartItem() {}
	
	/**
	 * Creates a new instance of a <code>ShoppingCartItem</code> with 
	 * the specified values.
	 * 
	 * @param title Title that represents this item.
	 * @param quantity The quantity of this item.
	 * @param price The price of the item.
	 */
	public ShoppingCartItem(String title, int quantity, double price) {
		setTitle(title);
		setQuantity(quantity);
		setPrice(price);
	}
	
	/*
	 * Getters & Setters
	 */
	
	/**
	 * Gets the title.
	 * @return Returns the title.
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Sets the title.
	 * @param title The new title.
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Gets the quantity of this item.
	 * @return Returns the quantity.
	 */
	public int getQuantity() {
		return quantity;
	}
	
	/**
	 * Sets the quantity of this item.
	 * @param quantity The new quantity.
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	/**
	 * Gets the price of this item.
	 * @return Returns the price.
	 */
	public double getPrice() {
		return price;
	}
	
	/**
	 * Sets a new price for this item.
	 * @param price The new price.
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	
	/**
	 * Returns a string that represents this item.
	 * This method is equivalent to getTitle.
	 */
	@Override
	public String toString() {
		return getTitle();
	}
	
	/**
	 * Compares this <code>ShoppingCartItem</code> to the specified object.
	 * If the specified object is a <code>ShoppingCartItem</code> they will be
	 * considered equal if they have the same title and price.
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ShoppingCartItem) {
			ShoppingCartItem item = (ShoppingCartItem) obj;
			return 
				item.getTitle().equals(getTitle()) &&
				item.getPrice() == getPrice();
		}
		return super.equals(obj);
	}
	
}
