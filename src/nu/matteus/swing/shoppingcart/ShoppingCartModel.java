package nu.matteus.swing.shoppingcart;

/**
 * Interface for models to be used by <code>ShoppingCart</code>s.
 * @author Matteus
 *
 */
public interface ShoppingCartModel extends Iterable<ShoppingCartItem> {
	
	/**
	 * Gets the number of items maintained by the model.
	 * @return Returns the number of items maintained by the model.
	 */
	public int size();
	
	/**
	 * Adds a <code>ShoppingCartItem</code> to the model.
	 * @param o Item to add.
	 */
	public void add(ShoppingCartItem o);
	
	/**
	 * Removes the specified item from the model.
	 * @param o The item to remove.
	 */
	public void remove(ShoppingCartItem o);
	
	/**
	 * Gets a <code>ShoppingCartItem</code> found on the specified index.
	 * @param index The index of the wanted item.
	 * @return Returns a <code>ShoppingCartItem</code>.
	 */
	public ShoppingCartItem get(int index);
	
	/**
	 * Subscribes the specified listener for events on this model.
	 * @param listener The listener to be notified with events from this model. 
	 */
	public void addShoppingCartModelListener(ShoppingCartModelListener listener);
	
}
