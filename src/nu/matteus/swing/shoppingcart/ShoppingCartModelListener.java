package nu.matteus.swing.shoppingcart;

/**
 * Interface for listeners of events by a <code>ShoppingCartModel</code>.
 * @author Matteus
 *
 */
public interface ShoppingCartModelListener {
	
	/**
	 * Fired when the model has added a new item.
	 * @param model Model that added the new item.
	 * @param item The new item that was added.
	 */
	void onShoppingCartModelItemAdded(ShoppingCartModel model, ShoppingCartItem item);
	
	/**
	 * Fired when a model has removed a item.
	 * 
	 * @param model The model that removed the item.
	 * @param item The item that was removed.
	 * @param index The previous model index of the item that was removed.  
	 */
	void onShoppingCartModelItemRemoved(ShoppingCartModel model, ShoppingCartItem item, int index);
	
	/**
	 * Fired when a item had its quantity changed by the model.
	 * 
	 * @param model The model that changed the quantity of a item.
	 * @param o The item that had its quantity changed.
	 * @param index The index of the item.
	 */
	void onShoppingCartModelItemQuantityChanged(
			DefaultShoppingCartModel model,
			ShoppingCartItem o, int index);
}
