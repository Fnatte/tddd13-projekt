package nu.matteus.swing.shoppingcart;

import java.util.Iterator;
import java.util.Vector;

/**
 * Default model for <code>ShoppingCart</code>.
 * Maintains a collection of <code>ShoppingCartItem</code>s and allows a few
 * methods to modify it (add, remove, etc.). Does also allow <code>ShoppingCartModelListener</code>
 * through <code>addShoppingCartModelListener</code>.
 * 
 * This model does not allow copies of items (compared using <code>ShoppingCartItem.equals()</code>),
 * instead of adding a multiple the previous item in the collection will have its quantity increased by one.
 * 
 * @author Matteus
 *
 */
public class DefaultShoppingCartModel implements ShoppingCartModel {

	/**
	 * Vector to hold all the ShoppingCartItems.
	 */
	private Vector<ShoppingCartItem> items;
	
	/**
	 * Vector to hold the listeners
	 */
	private Vector<ShoppingCartModelListener> listeners;
	
	/**
	 * Creates a new <code>DefaultShoppingCartModel</code>.
	 */
	public DefaultShoppingCartModel() {
		items = new Vector<>();
		listeners = new Vector<>();
	}
	
	/**
	 * Adds the specified <code>ShoppingCartItem</code>.
	 * If the item already exists, it will have its quantity increased by one.
	 */
	public void add(ShoppingCartItem o) {
		
		int index = items.indexOf(o);
		if(index >= 0) {
			ShoppingCartItem item = items.get(index);
			o.setQuantity(item.getQuantity()+1);
			item.setQuantity(item.getQuantity()+1);
			for(ShoppingCartModelListener listener : listeners) {
				listener.onShoppingCartModelItemQuantityChanged(this, item, index);
			}
			return;
		}
		
		items.add(o);
		
		for(ShoppingCartModelListener listener : listeners) {
			listener.onShoppingCartModelItemAdded(this, o);
		}
	}
	
	/**
	 * Removes the specified item. 
	 */
	public void remove(ShoppingCartItem o) {
		
		int index = items.indexOf(o);
		
		if(index >= 0) {
			items.remove(index);
			
			for(ShoppingCartModelListener listener : listeners) {
				listener.onShoppingCartModelItemRemoved(this, o, index);
			}
		}
		
	}
	
	/**
	 * Returns a iterator over all the items in this model.
	 */
	@Override
	public Iterator<ShoppingCartItem> iterator() {
		return items.iterator();
	}

	/**
	 * Returns the number of items maintained by this model.
	 */
	@Override
	public int size() {
		return items.size();
	}

	/**
	 * Returns the <code>ShoppingCartItem</code> found on the specified index.
	 */
	@Override
	public ShoppingCartItem get(int index) {
		return items.get(index);
	}

	/**
	 * Subscribes the specified listener for events from this model.
	 */
	@Override
	public void addShoppingCartModelListener(ShoppingCartModelListener listener) {
		listeners.add(listener);
	}

}
