package nu.matteus.swing.shoppingcart;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.NumberFormat;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * A <code>ShoppingCart</code> is a swing component is a collection
 * of chosen products. The user can (if not set otherwise) change the quantity
 * on each product in the collection. The <code>ShoppingCart</code> can also
 * display a footer with total cost.
 * 
 * @author Matteus
 *
 */
public class ShoppingCart extends JPanel {
	
	/*
	 * Fields
	 */
	
	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 3338107916881244451L;
	
	/**
	 * Model for the shopping cart
	 */
	private ShoppingCartModel model;
	
	/**
	 * Holds the index of the "last row" of components.
	 */
	private int currentRow;
	
	/**
	 * Panel that holds the "table" of items. 
	 */
	private JPanel content;
	
	/*
	 * Constructors
	 */
	
	/**
	 * Creates a new <code>ShoppingCart</code> using the specified model.
	 * @param model The model to be used by the <code>ShoppingCart</code>.
	 */
	public ShoppingCart(ShoppingCartModel model) {
		this.setModel(model);
		
		content = new JPanel(new GridBagLayout());
		add(content);
		
		addModelListener();
		createHeader();
	}

	/**
	 * Creates a new instance of a <code>ShoppingCart</code> with a default model.
	 */
	public ShoppingCart() {
		this(new DefaultShoppingCartModel());
	}

	/*
	 * Getter & Setters
	 */
	
	/**
	 * Gets the currently used model.
	 * @return Returns the model.
	 */
	public ShoppingCartModel getModel() {
		return model;
	}

	/**
	 * Sets the model to be used by this <code>ShoppingCart</code>.
	 * @param model ShoppingCartModel to be used.
	 */
	public void setModel(ShoppingCartModel model) {
		this.model = model;
	}
	
	
	/*
	 * Methods
	 */
	
	/**
	 * Creates the top header row with labels for each column. 
	 */
	private void createHeader() {
		addComponentWithConstraints(new JLabel("Cart Items"), 0, 0);
		addComponentWithConstraints(new JLabel("Quantity"), 1, 0);
		addComponentWithConstraints(new JLabel("Item Price"), 2, 0);
		addComponentWithConstraints(new JLabel("Total Price"), 3, 0);
	}

	/**
	 * Adds a anonymous listener to the listener that maps
	 * each event to methods on the <code>ShoppingCart</code>.
	 */
	private void addModelListener() {
		model.addShoppingCartModelListener(new ShoppingCartModelListener() {
			
			@Override
			public void onShoppingCartModelItemAdded(ShoppingCartModel model,
					ShoppingCartItem item) {
				addObjectComponent(item);
			}

			@Override
			public void onShoppingCartModelItemRemoved(ShoppingCartModel model,
					ShoppingCartItem item, int index) {
				removeObjectComponent(item, index);
			}

			@Override
			public void onShoppingCartModelItemQuantityChanged(
					DefaultShoppingCartModel defaultShoppingCartModel,
					ShoppingCartItem o, int index) {
				updateObjectComponent(o, index);
			}
		});
	}

	/**
	 * Adds a components to the content pane at the specified row and column.
	 * @param c Component to be added.
	 * @param x X-coordinate/column index
	 * @param y Y-coordinate/row index
	 */
	private void addComponentWithConstraints(Component c, int x, int y) {
		GridBagConstraints contraints = new GridBagConstraints(
			x, y, 1, 1, 0, 0,
			GridBagConstraints.PAGE_START,
			GridBagConstraints.HORIZONTAL,
			new Insets(3,10,3,10),
			0, 0
		);
		
		if(x == 0) contraints.ipadx = 40;
		
		content.add(
			c,
			contraints
		);
	}
	
	/**
	 * Adds a row on the content panel with components containing
	 * information specified by the provided <code>ShoppingCartItem</code>.
	 * 
	 * @param o <code>ShoppingCartItem</code> to represent as a row.
	 */
	private void addObjectComponent(final ShoppingCartItem o) {
		
		currentRow++;
		
		// Define all components for this row
		final JSpinner quantity;
		final JButton removeButton;
		final JPanel quantityPanel;
		final JLabel lblTitle;
		final JLabel lblItemPrice;
		final JLabel lblTotalPrice;
		
		// Initialize labels
		lblTitle = new JLabel(o.getTitle()); 
		lblItemPrice = new JLabel();
		lblTotalPrice = new JLabel();
		updatePriceLabels(o, lblItemPrice, lblTotalPrice);
		
		// Change font for labels
		Font labelFont = new Font(getFont().getFamily(), Font.PLAIN, 12);
		lblTitle.setFont(labelFont);
		lblItemPrice.setFont(labelFont);
		lblTotalPrice.setFont(labelFont);
		
		// Initialize quantity spinner
		SpinnerNumberModel numberModel = new SpinnerNumberModel(1, 1, 9999, 1);
		quantity = new JSpinner(numberModel);
		quantity.setAlignmentX(RIGHT_ALIGNMENT);
		quantity.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				o.setQuantity((int)quantity.getValue());
				updatePriceLabels(o, lblItemPrice, lblTotalPrice);
				repaint();
			}
		});
		
		// Initialize remove button
		removeButton = new JButton("Remove");
		removeButton.setAlignmentX(RIGHT_ALIGNMENT);
		removeButton.setBorderPainted(false);
		removeButton.setOpaque(false);
		removeButton.setContentAreaFilled(false);
		removeButton.setFocusable(false);
		removeButton.setFont(new Font(removeButton.getFont().getFamily(), Font.PLAIN, 11));
		removeButton.setText("<HTML><FONT color=\"#000099\"><U>Remove</U></FONT></HTML>");
		removeButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
		removeButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				removeButton.setText("<HTML><FONT color=\"#000000\"><U>Remove</U></FONT></HTML>");
			}
			@Override
			public void mouseExited(MouseEvent e) {
				removeButton.setText("<HTML><FONT color=\"#000099\"><U>Remove</U></FONT></HTML>");
			}
		});
		removeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.remove(o);
			}
		});
		
		// Initialize a panel for the quantity spinner and remove button
		quantityPanel = new JPanel();
		quantityPanel.setLayout(new BoxLayout(quantityPanel, BoxLayout.Y_AXIS));
		quantityPanel.add(quantity);
		quantityPanel.add(removeButton);
		
		// Add the components as a new row
		addComponentWithConstraints(lblTitle, 0, currentRow);
		addComponentWithConstraints(quantityPanel, 1, currentRow);
		addComponentWithConstraints(lblItemPrice, 2, currentRow);
		addComponentWithConstraints(lblTotalPrice, 3, currentRow);
		
		content.repaint();
		content.revalidate();
	}
	
	/**
	 * Updates the price on the specified labels by the provided <code>ShoppingCartItem</code>.
	 * Uses the current currency format of the current locale.  
	 * 
	 * @param item <code>ShoppingCartItem</code> to represent in the specified labels.
	 * @param lblItemPrice The label of which the item�s single price will be set as text.
	 * @param lblTotalPrice The label of which the item�s total price will be set as text.
	 */
	private void updatePriceLabels(ShoppingCartItem item, JLabel lblItemPrice, JLabel lblTotalPrice) {
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
		
		if(lblItemPrice != null) {
			String strItemPrice = currencyFormatter.format(item.getPrice());
			lblItemPrice.setText(strItemPrice);
		}
		
		if(lblTotalPrice != null) {
			String strTotalPrice = currencyFormatter.format(item.getQuantity() * item.getPrice());
			lblTotalPrice.setText(strTotalPrice);
		}
	}
	
	/**
	 * Removes the row that is represented by the specified <code>ShoppingCartItem</code>.
	 * @param o The <code>ShoppingCartItem</code> to remove.
	 * @param index The index of the specified <code>ShoppingCartItem</code> in the model.
	 */
	private void removeObjectComponent(ShoppingCartItem o, int index) {
		
		int componentIndex = getComponentIndex(index);
		content.remove(componentIndex);
		content.remove(componentIndex);
		content.remove(componentIndex);
		content.remove(componentIndex);
		
		content.repaint();
		content.revalidate();
		
	}
	
	/**
	 * Updates the specified <code>ShoppingCartItem</code> with its latest data.
	 * 
	 * @param item The <code>ShoppingCartItem</code> to update.
	 * @param index The index of the specified <code>ShoppingCartItem</code> in the model.
	 */
	private void updateObjectComponent(ShoppingCartItem item, int index) {
		int componentIndex = getComponentIndex(index);
		JPanel quantityPanel = (JPanel) content.getComponent(componentIndex + 1);
		JSpinner spinnerQuantity = (JSpinner) quantityPanel.getComponent(0);
		JLabel lblItemPrice = (JLabel) content.getComponent(componentIndex + 2);
		JLabel lblTotalPrice = (JLabel) content.getComponent(componentIndex + 3);
		spinnerQuantity.setValue(item.getQuantity());
		updatePriceLabels(item, lblItemPrice, lblTotalPrice);
		content.repaint();
	}
	
	/**
	 * Calculates the content panes component index by the specified model index.
	 * 
	 * @param modelIndex Model index of a <code>ShoppingCartItem</code>.
	 * @return Returns the calculated component index.
	 */
	private int getComponentIndex(int modelIndex) {
		return (modelIndex + 1) * 4;
	}
	
	/**
	 * Returns the preferred size of this <code>ShoppingCart</code>.
	 */
	@Override
	public Dimension getPreferredSize() {
		Dimension dim = super.getPreferredSize();
		dim.height = 400;
		return dim;
	}
	
}
